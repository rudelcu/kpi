<?php


$action=0;
if (isset($_GET['agg'])){
    $source =$_GET['agg'];
    if ($source==1){
        $action=1;
    }elseif ($source==2){
        $action=2;
    }elseif ($source==3){
        $action=3;
    }elseif ($source==4){
        $action=4;
    }
    
}

?>


<html lang="en">
<head>
<!--  Required meta tags always come first  -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Altran-TIM</title>
<!--  Font Awesome  -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
</head>

<body>

	<!--Main Navigation-->
	<header>
	    <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>
	</header>
	<!--Main Navigation-->

	<!--Main Layout-->
	<main>

	<div class="container">

		<!--Section: Blog v.4-->
		<section class="section section-blog-fw mt-5 pb-3 wow fadeIn">

			<!--Grid row-->
			<div class="row">
				<div class="col-md-12">
					<!--Featured image-->
					<div class="card card-cascade wider ">
						<div class="view overlay hm-white-slight">
							<img class="center" src="img/timlogo.png"
								alt="Wide sample post image"> <a>
								<div class=""></div>
							</a>
						</div>

						<!--Post data-->
						<div class="card-body text-center">
							<h2>
								<a><strong>Lista KPI: Fonte NGASP </strong></a>
							</h2>
							<p>
								A cura di <a>TimLabMultimedia</a>, 23/04/2018
							</p>

							<!--Social shares-->
							<div class="social-counters ">

								
							</div>
							<!--Social shares-->

						</div>
						<!--Post data-->
					</div>

					<!--Excerpt-->
					<div class="excerpt mt-5 wow fadeIn" data-wow-delay="0.3s"></div>
				</div>
			</div>
			<!--Grid row-->

		</section>
		<!--Section: Blog v.4-->

		<hr class="mb-5 mt-4">

		<!--Section: Blog v.2-->
		<section id="aggFW" class="section extra-margins text-center pb-3 wow fadeIn" style="display:none">

			<!--Section Agg FW-->
			<!--Grid row-->
			<br>
			<div class="container" id="container_skipper">
			<br>
    			<p class="text"><b>SKIPPER</b></p>
    			<div class="row col-12" id="skipper_view">
    				<div class="col-sm-3">
    				</div>
    				<div class="col-sm-3">
    					<label for="skipper_tot">Totale Skipper Aggiornati</label> <input
    						type="text" class="form-control form-control-lg text-center"
    						id="skipper_tot" readonly>
    
    				</div>
    				<div class="col-sm-3">
    					<label for="skipper_tot_cb">% Aggiornati su CB</label> <input
    						type="text" class="form-control form-control-lg text-center"
    						id="skipper_tot_cb" readonly>
    				</div>
    				<div class="col-sm-3">
    				</div>
    			</div>
    			<br>
			</div>
			<br>
			<div class="container-fluid" id="container_timbox">
			<br>
			<p class="text"><b>TIMBOX</b></p>
			<div class="row col-12" id="timbox_view">
				<div class="col-sm-3">
				</div>
				<div class="col-sm-3">
					<label for="timbox_tot">Tot TIMBOX Aggiornati</label> <input
						type="text" class="form-control form-control-lg text-center"
						id="timbox_tot" readonly>
				</div>
				<div class="col-sm-3">
					<label for="timbox_tot_cb">% Aggiornati su CB</label> <input
						type="text" class="form-control form-control-lg text-center"
						id="timbox_tot_cb" readonly>
				</div>
				<div class="col-sm-3">
				</div>
			</div>
			<br>
			</div>
			<br>

			<div class="row"></div>
			<!--Grid row-->
		
		</section>
		<!--Fine Section Agg FW-->
		
		<section id="storico" class="section extra-margins text-center pb-3 wow fadeIn">
			<div clas="row  col-12" id="storico_timbox">
					<label for = "lineChart_timbox">Storico Aggiornamenti TIMBOX</label>
						<p class="blue-grey-text">Per il dettaglio gruppi &egrave possibile selezionare la linea che si vuole escludere dalla legenda</p>
						<canvas id="lineChart_timbox"></canvas>
					
					
			</div>
			<br>
			<div clas="row  col-12" id="storico_skipper">
				<label for = "lineChart_skipper">Storico Aggiornamenti Skipper</label>
				<p class="blue-grey-text">Per il dettaglio gruppi &egrave possibile selezionare la linea che si vuole escludere dalla legenda</p>
					<canvas id="lineChart_skipper"></canvas>
				</div>
		</section>

		<!--Section aggiornamenti APP-->
		<section id="aggAPP" class="section extra-margins text-center pb-3 wow fadeIn" style="display=none">
			
			<div class="row col-12" id="app_view">
    			<div class="col-md-4">
    							 <select
    								class="mdb-select colorful-select dropdown-primary border_custom_select"
    								id="selectAPP">
    								<option value="1">Sel</option>
    								<option value="TIMVISIONLiveTV-5.5.2">TV APP</option>
    								<option value="TIMVISION-10.10.11">TIMvision</option>
    								<option value="Google Play Giochi-5.8.53 (197013620.197013620-000306">Account Google (es. per Carlo)</option>
    							</select>
    						</div>
    			<div class="col-md-4">
    				<label for="form1" class="">PreloadedAPPversion</label>
        				 <input type="text" id="preloadedAPPversion" class="form-control form-control-sm">
       					 
    				
    			</div>
    			<div class="col-md-4">
    				<button type="button" class="btn btn-primary btn-rounded waves-light" onclick="calcolaAGG()">Calcola</button>
    			</div>
			</div>
			<br>
			<div class="text-lg-center">SKIPPER			
			</div>
			<div class="row col-12" id="result_skipper">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<label for="app_aggiornate">Aggiornati </label> <input
						type="text" class="form-control form-control-lg text-center"
						id="app_aggiornate" readonly>
				</div>
				<br>
				<div class="col-md-3">
					<label for="app_aggiornate">% Aggiornati su Attivi</label> <input
						type="text" class="form-control form-control-lg text-center"
						id="app_aggiornate_perc" readonly>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			<br>
			<div class="text-center">TIMBOX			
			</div>
			<div class="row col-12" id="result_timbox">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<label for="app_aggiornate_timbox">Aggiornati </label> <input
						type="text" class="form-control form-control-lg text-center"
						id="app_aggiornate_timbox" readonly>
				</div>
				<br>
				<div class="col-md-3">
					<label for="app_aggiornate_timbox">% Aggiornati su Attivi</label> <input
						type="text" class="form-control form-control-lg text-center"
						id="app_aggiornate_perc_timbox" readonly>
				</div>
				<div class="col-md-3">
				</div>
			
		</section>
		<!--/.Fine aggiornamenti app-->
		<!--/.Inizio utilizzo app-->
		<section id="utilizzoAPP" class="section extra-margins text-center pb-3 wow fadeIn" style="display=none">
			<div clas="row  col-12" id="utilizzo_timbox">
					<label for = "lineChart_timbox">TIMBOX</label>
						<p class="blue-grey-text">Per il dettaglio &egrave possibile selezionare la barra che si vuole escludere dalla legenda</p>
						<canvas id="utilizzoAPP_timbox";"></canvas>
					
					
			</div>
			<br>
			<div clas="row  col-12" id="utilizzo_skipper">
				<label for = "lineChart_skipper">Skipper</label>
				<p class="blue-grey-text">Per il dettaglio  &egrave possibile selezionare la linea che si vuole escludere dalla legenda</p>
					<canvas id="utilizzoAPP_skipper"></canvas>
			</div>
		</section>
		<!--/.Fine utilizzo app-->

		<!--Section: Comments list-->
		<section class="mb-4 pt-5 wow fadeIn" data-wow-delay="0.3s">

			<!--Main wrapper-->
			<div class="comments-list text-md-left text-center">
				<div class="section-heading"></div>
				<!--First row-->
				<div class="row">
					<!--Image column-->
					<div class="col-sm-2 col-12"></div>
					<!--/.Image column-->

					<!--Content column-->
					<div class="col-sm-10 col-12"></div>
					<!--/.Content column-->

				</div>
				<!--/.First row-->

				<!--Second row-->
				<div class="row">
					<!--Image column-->
					<div class="col-sm-2 col-12"></div>
					<!--/.Image column-->

					<!--Content column-->
					<div class="col-sm-10 col-12"></div>
					<!--/.Content column-->

				</div>
				<!--/.Second row-->

				<!--Third row-->
				<div class="row">

					<!--Image column-->
					<div class="col-sm-2 col-12"></div>
					<!--/.Image column-->
					<!--Content column-->
					<div class="col-sm-10 col-12"></div>
					<!--/.Content column-->
				</div>
				<!--/.Third row-->
			</div>
			<!--/.Main wrapper-->

		</section>
		<!--/Section: Comments list-->


		<!--Section: Leave a reply (Logged In User)-->

		<!--/Section: Leave a reply (Logged In User)-->

	</div>

	</main>
	<!--Main Layout-->

	<!--Footer-->
	<footer class="page-footer indigo center-on-small-only pt-0">

		<!--Footer Links-->
		<div class="container">

			<!--First row-->
			<div class="row">

				<!--First column-->
				<div class="col-md-12 wow fadeIn" data-wow-delay="0.3s">

					<div class="text-center d-flex justify-content-center my-4">

						<!--Facebook-->
						<a class="icons-sm fb-ic"><i
							class="fa fa-facebook white-text fa-lg pr-md-4"> </i></a>
						<!--Twitter-->
						<a class="icons-sm tw-ic"><i
							class="fa fa-twitter white-text fa-lg pr-md-4"> </i></a>
						<!--Google +-->
						<a class="icons-sm gplus-ic"><i
							class="fa fa-google-plus white-text fa-lg pr-md-4"> </i></a>
						<!--Linkedin-->
						<a class="icons-sm li-ic"><i
							class="fa fa-linkedin white-text fa-lg pr-md-4"> </i></a>
						<!--Instagram-->
						<a class="icons-sm ins-ic"><i
							class="fa fa-instagram white-text fa-lg pr-md-4"> </i></a>
						<!--Pinterest-->
						<a class="icons-sm pin-ic"><i
							class="fa fa-pinterest white-text fa-lg pr-md-4"> </i></a>
					</div>

				</div>
				<!--/First column-->

			</div>
			<!--/First row-->

		</div>
		<!--/Footer Links-->

		<!--Copyright-->
		<div class="footer-copyright wow fadeIn" data-wow-delay="0.3s">
			<div class="container-fluid">
				&copy; 2018 Copyright: <a href="https://www.MDBootstrap.com">
					MDBootstrap.com </a>

			</div>
		</div>
		<!--/Copyright-->

	</footer>
	<!--/Footer-->

	<!--  SCRIPTS  -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script src='js/charts.js'></script>
	<script src='js/loadingoverlay.min.js'></script>
	<script src='js/navigation.js'></script>

	<script>
	$(document).ready(function () {
		//hideComponent('spinner');
		//showComponent('completed');
    	$('.mdb-select').material_select();
    	//$('#spinner').LoadingOverlay("hide");      	
    });
	new WOW().init();
	sectionVisibility('aggAPP',false); 
	//$('.mdb-select').material_select();
	initNavBar();
	// action='0';
	
	
	 var action= <?php echo $action; ?>;
	 if(action == 0){
     	
	 }else if (action == 1) {
		 //AGG FW
		sectionVisibility('aggFW',true);
		sectionVisibility('storico',false);
		sectionVisibility('aggAPP',false);
		sectionVisibility('utilizzoAPP',false);
		$.ajax({
			type : 'post',
			url : 'php/controller.php',
			data : {
				'source' : 'queryAggFW',
			},
			success : function(response) {
				//var result=response.split('|');
				console.log(response);
				
				var tot_cb=386585;
				
				//$('#skipper_rollOut_p').val((rollOut/rollOut_cb*100).toFixed(2));
				//showCharts(result_new);
				  //$('#skipper_view').LoadingOverlay("hide");
				  //$('#skipper_view_percentuali').LoadingOverlay("hide");
			},
			error : function() {
				alert("error");
			}
		});
		} else if (action == 2) {
			//AGG APP
			sectionVisibility('aggFW',false);
			sectionVisibility('storico',false);
			sectionVisibility('aggAPP',true);
			sectionVisibility('utilizzoAPP',false);
		}else if (action == 3) {
			//Utilizzo APP
			sectionVisibility('aggFW',false);
			sectionVisibility('storico',false);
			sectionVisibility('aggAPP',false);
			sectionVisibility('utilizzoAPP',true);

			$.ajax({
				type : 'post',
				url : 'php/controller.php',
				data : {
					'source' : 'utilizzoAPP',
				},
				success : function(response) {
					console.log(response);
					charts_utilizzoAPP('utilizzoAPP_timbox',response);
				},
				error : function() {
					alert("error");
				}
			});
			
			
		}else if (action == 4){
			//AGG DATI
			$.ajax({
			type : 'post',
			url : 'php/controller.php',
			data : {
				'source' : 'AggiornaDB',
			},
			success : function(response) {
				console.log(response);
				
			},
			error : function() {
				alert("error");
			}
		});
		}
						
	

	
	
    </script>
</body>
</html>