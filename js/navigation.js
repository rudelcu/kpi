/* includere nella pagina php:
 * include 'config.inc.php';
 * 
 * includere nella pagina php lato html:
	<!--Main Navigation-->
	<header>
	    <nav class="navbar navbar-expand-lg navbar-dark indigo" id="navBar"></nav>
	</header>
    <!--/Main Navigation--> 
 * 
 * lato JS:
 * initNavBar('<?php echo $baseurl;?>');
 */

function initNavBar() {
	//var path= "http://79.7.35.24/KPI"; 
	var path= "http://localhost/KPI";
	var version = '1.0';
	var html = '<!-- Navigation Bar Start -->';
	html += '<div class="container">';
	html += '<a class="navbar-brand active" href="'
			+ path
			+ '"><i class="fa fa-home"></i><strong> KPI - <span id="small-font">Ver. '
			+ version
			+ '</span></strong><span class="sr-only">(current)</span></a>';
	html += '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">';
	html += '<span class="navbar-toggler-icon"></span>';
	html += '</button>';
	html += '<div class="collapse navbar-collapse" id="navbarSupportedContent">';
	html += '<ul class="navbar-nav mr-auto">';
	html += '<li class="nav-item">';
	html += '<a class="nav-link" href="'
			+ path
			+ '/index.php?agg=1"><i class="fa fa-users"aria-hidden="true"></i> Aggiornamenti FW</a>';
	html += '</li>';
	html += '<li class="nav-item">';
	html += '<a class="nav-link" href="'
			+ path
			+ '/index.php?agg=2"><i class="fa fa-id-card-o" aria-hidden="true"></i> Aggiornamenti APP</a>';
	html += '</li>';
	html += '<li class="nav-item">';
	html += '<a class="nav-link" href="'
			+ path
			+ '/index.php?agg=3"><i class="fa fa-users"aria-hidden="true"></i> Utilizzo APP</a>';
	html += '</li>';
	html += '<li class="nav-item">';
	html += '<a class="nav-link" href="'
			+ path
			+ '/index.php?agg=4"><i class="fa fa-database"aria-hidden="true"></i> AGGIORNA I DATI</a>';
	html += '</li>';
	html += '</ul>';
	html += '</div>';
	html += '<!-- Navigation Bar End-->';
	$("#navBar").html(html);
}

function sectionVisibility(sectionId,boolean){
		if (boolean==true){
			$("#" + sectionId).show();
		}
		else{
			$("#" + sectionId).hide();
		}

	
}

function calcolaAGG(){
	var selectValue=$('#selectAPP').val();
	var preloadedAPPversion=$('#preloadedAPPversion').val();
	if(preloadedAPPversion!==""){
		query=preloadedAPPversion;
	}else{
		if(selectValue!=="1"){
			query=selectValue;
		}else {
			toast.warning("selezionare un'app da monitorare").show();
		}
	}
	
	if(query!==undefined){
		$.ajax({
			type : 'post',
			url : 'php/function.php',
			data : {
				'preloaded_Skipper' : query,
			},
			success : function(response) {
				console.log(response);
				var result=response.split('|');
				var result_preloaded=parseInt(result[0]);
				var result_unici=parseInt(result[1]);
				var result_preloaded_timbox=parseInt(result[2]);
				var result_unici_timbox=parseInt(result[3]);
				$('#app_aggiornate').val(result_preloaded);
				$('#app_aggiornate_perc').val((result_preloaded/result_unici*100).toFixed(2));
				$('#app_aggiornate_timbox').val(result_preloaded_timbox);
				$('#app_aggiornate_perc_timbox').val((result_preloaded_timbox/result_unici_timbox*100).toFixed(2));
			},
			error : function() {
				alert("error");
			}
		});
	}
	
}