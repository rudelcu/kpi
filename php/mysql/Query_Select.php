<?php

require '../config.php';
require '../db_connection.php';


class App {
    public $version = "";
    public $package  = "";
    public $url = "";
}

//esempio di url che va passato per avere il json delle app da aggiornare via ota
//isifast.dyndns.org:10080/ota/response.php?modelName=TIM_BOX&os=os&serialNumber=123456789
//esempio di url che va passato per avere il json dei cluster presenti
//isifast.dyndns.org:10080/ota/response.php?modelName=null&os=null&serialNumber=null

//get data from URL
//if (isset($_POST['query']) && !empty($_POST['query'])) {
$query = $_POST['query'];
	//echo "Yes, mail is set";    
//}

//Jeson result with all cluster and their app
$obj = [];//new stdClass();

$result = mysqli_query($conn, $query);

$rows = array();
while($r = mysqli_fetch_assoc($result)) {
    $rows[] = $r;
}
print json_encode($rows);

mysqli_close($conn);

?>
