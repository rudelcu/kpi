<?php

include 'db_connection.php';




//esempio di url che va passato per avere il json delle app da aggiornare via ota
//isifast.dyndns.org:10080/ota/response.php?modelName=TIM_BOX&os=os&serialNumber=123456789
//esempio di url che va passato per avere il json dei cluster presenti
//isifast.dyndns.org:10080/ota/response.php?modelName=null&os=null&serialNumber=null

//get data from URL
//if (isset($_POST['query']) && !empty($_POST['query'])) {
// $query = $_POST['query'];
// 	//echo "Yes, mail is set";    
// //}

// //Jeson result with all cluster and their app
// $obj = [];//new stdClass();

// $result = mysqli_query($conn, $query);

// $rows = array();
// while($r = mysqli_fetch_assoc($result)) {
//     $rows[] = $r;
// }
// print json_encode($rows);

// mysqli_close($conn);


function checkFileAndInsert() {


    require 'db_connection.php';
    
    //$dir = './upload_file';
    
    $dir = '../v2';
    $files1 = scandir($dir);
    
    //check nuovi file dalla cartella e salvataggio in quella temporanea
    $insert_err=0; //controllo eventuali errori sull'insert
    $a=0;
    $temp=[];
    for ($i=2;$i<sizeof($files1);$i++){
        $nome_file= substr($files1[$i], 20,8);
        $check_v2="SELECT file from `check_file` WHERE file like '%".$nome_file."%'";
        
        $result=mysqli_query($conn,$check_v2);
        //print_r($conn);
        if ($result)
        {
            // Return the number of rows in result set
            $rowcount=mysqli_num_rows($result);
            if ($rowcount==0){
                $temp[$a]=$files1[$i];
                $a++;
            }
            // Free result set
            mysqli_free_result($result);
        }
        
        
        
    }
    
    for ($i=0;$i<sizeof($temp);$i++){
        /* unset($csv_array_fw);
        unset($csv_array_preloaded);
        unset($csv_array_svopen);
        $csv_array_fw = Array();
        $csv_array_preloaded = Array();
        $csv_array_svopen= Array(); */
        
        $new_path=$dir."/".@$temp[$i];
        
        $file = @fopen("$new_path", 'r');
        if($file){
            $fileLog = fopen("./log/logfile_".substr($temp[$i], 0,-4).".txt","wb");
            $temp_log_upgrade=0;
            $temp_log_preloaded=0;
            $temp_log_sv=0;
            //$current=file_get_contents("./log/logfile_".substr($temp[$i], 0,-4).".txt");
            while (($line = fgetcsv($file)) !== FALSE) {
                //$line is an array of the csv elements
                //$line[0]=";".$line[0];
                // Divido il csv per Eventi
                //1. Upgrade FW
                //$current.=""
                if (strpos($line[0], 'SYSTEM_UPGRADE')){
                    
                    //array_push($csv_array_fw,$line);
                    
                    $result=explode(';',$line[0]);
                    $insert_query = "INSERT INTO `upgrade_timbox` (`CPEID`, `Event Name`, `FW Version`, `timestamp` ) VALUES ('".$result[0]."','".$result[1]."','".$result[2]."','".$result[3]."') ON DUPLICATE KEY UPDATE  `CPEID` = `CPEID`";
                    $result_query=mysqli_query($conn, $insert_query);
           
                    $temp_log_upgrade++;
                    if(!$result_query){
                        
                        fwrite($fileLog,"SYSTEM_UPGRADE ".$temp_log_upgrade. " ERROR \n");
                        $err=mysqli_error();
                        $insert_err++;
                        break;
                        
                    }else  fwrite($fileLog,"SYSTEM_UPGRADE ".$temp_log_upgrade. " OK \n");
                  
                    
                }elseif (strpos($line[0], 'PRELOADED_UPGRADE')){ //1. Upgrade
                    
                    $result=explode(';',$line[0]);
                    //print_r($result);
                    
                    $insert_query = "INSERT INTO `preloaded_timbox` (`CPEID`, `Event Name`, `FW Version`, `timestamp`,`preloadedAppVersion` ) VALUES ('".$result[0]."','".$result[1]."','".$result[2]."','".$result[3]."','".$result[6]."')";
                    
                    $result_query=mysqli_query($conn, $insert_query);
                    
                    $temp_log_preloaded++;
                    if(!$result_query){
                        fwrite($fileLog, "PRELOADED_UPGRADE ".$temp_log_preloaded. " ERROR \n");
                        $err=mysqli_error();
                        $insert_err++;
                        break;
                    }else fwrite($fileLog,"PRELOADED_UPGRADE ".$temp_log_preloaded. " OK \n");
                    
                    //array_push($csv_array_preloaded,$line);
                }elseif (strpos($line[0], 'SV_OPEN')){ //1. Upgrade
                    //array_push($csv_array_svopen,$line);
                    $result=explode(';',$line[0]);
                    //print_r($result);
                    
                    $insert_query = "INSERT INTO `sv_open_timbox` (`CPEID`, `Event Name`, `FW Version`, `timestamp`,`servId`,`version` ) VALUES ('".$result[0]."','".$result[1]."','".$result[2]."','".$result[3]."','".$result[4]."','".$result[5]."')";
                    
                    $result_query=mysqli_query($conn, $insert_query);
                    $temp_log_sv++;
                    if(!$result_query){
                        fwrite($fileLog,"SV_OPEN ".$temp_log_sv. " ERROR \n");
                        $err=mysqli_error();
                        $insert_err++;
                        break;
                    }else fwrite($fileLog,"SV_OPEN ".$temp_log_sv. " OK \n");
                    
                }
                
                
            }
            //fwrite($fileLog, $current);
            fclose($file);
            
            
            //rinominare il file
        }
        
        //insert nome file nella tabella di check
        $insert_query = "INSERT INTO `check_file` (`file`) VALUES ('".$temp[$i]."')";
        mysqli_query($conn, $insert_query);
        
        
       
        //insert file in tabella check per evitare successivi insert
        if ($insert_err!=0){ //variabile controllo errori su insert
            $insert_query = "DELETE FROM `check_file` WHERE `file` ='".$temp[$i]."'";
            mysqli_query($conn, $insert_query);
            print $err;
        }else{
            print "OK!";
        }    
        
    }
    mysqli_close($conn);
}

function utilizzoAPP($device){
    require 'db_connection.php';
    //query giornaliere
    $apps=['Timvision%\' or servId LIKE \'%TV_','Timgames','Netflix','Dazn', 'App','Youtube','Sensi'];
    $apps_name=['Timvision','Timgames','Netflix','Dazn', 'App e Giochi','Youtube','Sensi Unici'];
    $pippo=Array();
    $data_oggi= date('Y-m-d');
    $i =0;
    for ($i; $i<sizeof($apps); $i++){
        $query="SELECT COUNT(*) as '".$apps_name[$i]."' FROM sv_open_".$device." WHERE timestamp LIKE '%".$data_oggi."%' and servId LIKE '%".$apps[$i]."%'";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_row($result);
        //echo $row->$apps[$i];
        $pippo[$apps_name[$i]]=$row[0];
    }
        mysqli_close($conn);
        print json_encode($pippo);
}

?>
